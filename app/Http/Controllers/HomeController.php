<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Funcionario;
use App\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider=Slider::where('status','=','active')->paginate(1);

        $categorias = Categoria::all();
        $funcionarios = Funcionario::orderBy('nome','asc')->get();



        return view('layouts.frontend.home')->withSlider($slider)->withCategorias($categorias)->withFuncionarios($funcionarios);
    }
}
