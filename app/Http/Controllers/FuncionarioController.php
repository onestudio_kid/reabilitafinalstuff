<?php

namespace App\Http\Controllers;

use App\Funcionario;
use App\Http\Requests\funcionarioRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use Auth;
use Session;

class FuncionarioController extends Controller
{


    public function __contruct(){
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Funcionario $funcionarios)
    {
       if($request){
        $query = trim($request->Procurar);
        $funcionario = Funcionario::where('nome', 'LIKE', '%'.$query.'%')
        ->orderBy('id','desc')
        ->paginate(8);
        if ($funcionario != null) {
          return view('layouts.backend.admin.funcionario.index')->withFuncionarios($funcionario)->withProcurar($query);
      }




  } 
  return view('layouts.backend.admin.funcionario.index');
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('layouts.backend.admin.funcionario.create');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(funcionarioRequest $request, Funcionario $user)
    {
        if(Input::file())
        {

            $image = Input::file('avatar');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('assets/Profile/' . $filename);


            Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $user->avatar = $filename;
            $user->nome = ucfirst($request->nome);
            $user->apelido = ucfirst($request->apelido);
            $user->profissao = ucfirst($request->profissao);
            $user->sexo = ucfirst($request->sexo);
            $user->registado_por = Auth::user()->name;     

            $user->save();
            Session::flash('success',"Funcionario $request->nome $request->apelido guardado com");
        }

        return redirect()->route('funcionario.index');  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $funcionario= Funcionario::findOrFail($id);
        return view('layouts.backend.admin.funcionario.show')
        ->withFuncionario($funcionario);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
        $funcionario=Funcionario::findOrFail($id);
        return view('layouts.backend.admin.funcionario.edit')->withFuncionario($funcionario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)

    {

        $user=Funcionario::findOrFail($id);
        if(Input::file())
        {

            $image = Input::file('avatar');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('assets/Profile/' . $filename);


            Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $user->avatar = $filename;
            $user->nome = ucfirst($request->nome);
            $user->apelido = ucfirst($request->apelido);
            $user->profissao = ucfirst($request->profissao);
            $user->sexo = ucfirst($request->sexo);    
            $user->atualizado_por = Auth::user()->name;     

            $user->update();
            Session::flash('success',"Funcionario $request->nome $request->apelido Atulizado com");
        }

        else{
            $user->nome = ucfirst($request->nome);
            $user->apelido = ucfirst($request->apelido);
            $user->profissao = ucfirst($request->profissao);
            $user->sexo = ucfirst($request->sexo);    
            $user->atualizado_por = Auth::user()->name;     

            $user->save();
            Session::flash('success',"Funcionario $request->nome $request->apelido Atulizado com");



        }
        return redirect()->route('funcionario.index');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Funcionario::destroy($id);
        Session::flash('success',"Funcionario $request->nome $request->apelido Eliminado com");
        return redirect()->route('funcionario.index');
    }
}
