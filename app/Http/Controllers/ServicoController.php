<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\requestServico;
use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Purifier;

class ServicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Servico $servico, Request $request)
    {
        if($request){
            $query = trim($request->Procurar);
            $servicos=Servico::where('nome','LIKE', '%'.$query.'%')
            ->orderBy('nome')
            ->paginate(8);

            if($servico =! null){
              return view('layouts.backend.admin.servico.index')->withServicos($servicos)->withProcurar($query);
          }
      }
      
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();
        return view('layouts.backend.admin.servico.create')->withCategorias($categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(requestServico $request, Servico $servico)
    {
        $servico->nome = ucwords($request->nome);
        $servico->detalhes = ucfirst($request->detalhes);
        $servico->categoria_id = $request->categoria_id;
        $servico->registado_por = Auth::user()->name;
        $servico->save();


        Session::flash('success'," Serviço $request->nome Guardado com ");
        return redirect()->route('servico.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function show(Servico $servico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servico = Servico::findOrfail($id);
        $cat = Categoria::all();
        $categorias = [];

        foreach ($cat as $cat) {
            $categorias[$cat->id] = $cat->nome;

        }

        return view('layouts.backend.admin.servico.edit')->withServico($servico)
        ->withCategorias($categorias);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id)
    { 

        $servico = Servico::findOrfail($id);




        if ($servico->nome != $request->nome) {
         $this->validate($request,[
            'nome'=>'required|min:5|max:255|unique:servicos',
            'detalhes'=>'required|min:5'
        ]);
     }else
     {
      $this->validate($request,[
        'nome'=>'required|min:5|max:255',
        'detalhes'=>'required|min:5'
    ]);

  }

  $servico->nome = ucwords($request->nome);
  $servico->detalhes = Purifier::clean($request->detalhes);
  $servico->categoria_id = $request->categoria_id;

  $servico->atualizado_por = Auth::user()->name;
  $servico->update();


  Session::flash('success'," Serviço $request->nome Atualizado  com ");
  return redirect()->route('servico.index');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servico  $servico
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        Servico::destroy($id);
        Session::flash('success',"categoria  eliminado com ");
        return redirect()->route('servico.index');
    }
}
