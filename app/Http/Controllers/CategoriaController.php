<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\requestCategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Image;
use Auth;
class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Categoria $categoria)


    {
        if($request){
            $query = trim($request->Procurar);
            $categoria = Categoria::where('nome','LIKE','%'.$query.'%')
            ->orderby('nome','asc')
            ->paginate(6);

            if($categoria != null){
               return view('layouts.backend.admin.categoria.index')->withCategorias($categoria)->withProcurar($query);

           }
       }

       return view('layouts.backend.admin.categoria.index');
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.backend.admin.categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(requestCategoria $request, Categoria $categoria)
    {
        if(Input::file())
            {
                $image= Input::file('avatar');
                $filename = time().'.'.$image->getClientOriginalExtension();
                $path = public_path('assets/Servicos/'.$filename);
                Image::make($image->getRealPath())->resize(954,683)->save($path); 
                $categoria->avatar = $filename;
                $categoria->nome = ucwords($request->nome);
                $categoria->registado_por =Auth::user()->name;
                $categoria->atualizado_por = null;
                $categoria->save();


                Session::flash('success',"Categoria $request->nome  guardado com");
                return redirect()->route('categoria.index');
            }
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria=Categoria::findOrFail($id);

        return view('layouts.backend.admin.categoria.edit')->withCategoria($categoria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $categoria=Categoria::findOrFail($id);
        $oldName = $categoria->nome;

        if($oldName != $request->nome){

            $this->validate($request,array(
                'nome'=>'required|min:5|max:255|unique:categorias'
            ));

        } else


        {
            $this->validate($request,array(
                'nome'=>'required|min:5|max:255|'
            ));
        }

        if(Input::file())
            {
                $image= Input::file('avatar');
                $filename = time().'.'.$image->getClientOriginalExtension();
                $path = public_path('assets/Servicos/'.$filename);
                Image::make($image->getRealPath())->resize(954,683)->save($path); 
                $categoria->avatar = $filename;
                $categoria->nome = ucwords($request->nome);

                $categoria->atualizado_por = Auth::user()->name;
                $categoria->update();


                Session::flash('success',"Categoria $request->nome  guardado com");
                return redirect()->route('categoria.index');
            }
            else{
              $categoria->nome = ucwords($request->nome);
              $categoria->atualizado_por = Auth::user()->name;
              $categoria->update();

              Session::flash('success',"Categoria $request->nome  guardado com");
              return redirect()->route('categoria.index');

          }




          return redirect()->route('categoria.index');
      }


      public function updateImage(Request $request ,Categoria $categoria){

       Session::flash('success',"Imagem Atualizada com");
       return redirect()->route('categoria.index');
   }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categoria::destroy($id);
        Session::flash('success',"Categoria Eliminada com");
        return redirect()->route('categoria.index');
    }
}
