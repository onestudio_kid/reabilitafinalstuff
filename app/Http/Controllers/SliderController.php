<?php

namespace App\Http\Controllers;

use App\Http\Requests\sliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;
use Purifier;
use Input;

class SliderController extends Controller
{
  public function __contruct(){
   $this->middleware('auth');
}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Slider $slider, Request $request)

    {


      if($request){
        $query = trim($request->Procurar);
        $sliders = Slider::where('titulo', 'LIKE', '%'.$query.'%')
        ->orderBy('id','desc')
        ->paginate(4);
        if ($sliders != null) {
          return view('layouts.backend.admin.slider.index')->withSliders($sliders)->withProcurar($query);
      }


      if (Request::ajax()) 
        {
          return Response::json($sliders);
      }

  } 

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('layouts.backend.admin.slider.create');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(sliderRequest $request, Slider $slider)
    {
      

      if(Input::file())
        

        if(Input::file()){
         

         $image = Input::file('img');
         $fileName = time().'.'.$image->getClientOriginalExtension();
         $path = public_path('assets/Sliders'.$fileName);

           // Image::make($fileName)->resize('1600,850');
         Image::make($image->getRealPath()->resize(1600,850)->save($path));
         $slider->imagem = $fileName;
         $slider->titulo = ucwords($request->titulo);
         $slider->descricao = Purifier::clean($request->descricao);
         $slider->criado_por = Auth::user()->name;
         $slider->atualizado_por = Auth::user()->name;
            /**
             * verificar o estado do ultimo slide e atribuir
             * nulo para que o novo receba a propriedade active
             */
            // if($request->has('status')){
            //     $activeSlider = Slider::find(Slider::all()->last()->id);
            //     $activeSlider->status = null;
            //     $activeSlider->save();

            // }

            $slider->status = strtolower($request->status);
            $slider->save();
            Session::flash('success',"Slider com o titulo  $request->titulo guardado com");
        }

        return redirect()->route('slider.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     return view('layouts.backend.admin.slider.show')->withSlider(Slider::findOrFail($id));
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $slider=Slider::findOrFail($id);
      return view('layouts.backend.admin.slider.edit')->withSlider($slider);
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
     $slider=Slider::findOrFail($id); 
     $old=$slider->titulo;



     if ($slider->titulo != $request->titulo) {
       $this->validate($request,[
        'titulo'=>'required|min:5|max:255|unique:sliders',
        'descricao'=>'required|min:5|max:255'
    ]);
   }else
   {
      $this->validate($request,[
        'titulo'=>'required|min:5|max:255',
        'descricao'=>'required|min:5|max:255'
    ]);

  }

  $slider->titulo = ucwords($request->titulo);
  $slider->descricao = Purifier::clean($request->descricao);
  $slider->atualizado_por = Auth::user()->name;

  $slider->save();
  Session::flash('success',"Slider com o titulo $old atualizado para  $request->titulo com");

  return redirect()->route('slider.index');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


      if(Slider::destroy($id)){
       Session::flash('success',"Slider  eliminado com");
   }

   return redirect()->route('slider.index');

}
}
