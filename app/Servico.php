<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
	protected $fillable=['nome','detalhes','categoria_id','registada_por'];


/**
 * Relação entre tabelas
 * @return [relaçao] [ 1 para muitos]
 */
public function categoria(){
	return $this->belongsTo('App\Categoria');
}

}
