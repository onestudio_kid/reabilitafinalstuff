<!DOCTYPE html>
<html>
<head>
  <title>Reabilita | @yield('title') </title>


  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->

  {{ html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
  <!-- Font Awesome -->
  {{ html::style('bower_components/font-awesome/css/font-awesome.min.css') }}
  <!-- Ionicons -->
  {{ html::style('bower_components/Ionicons/css/ionicons.min.css') }}
  <!-- Theme style -->
  {{ html::style('dist/css/AdminLTE.min.css') }}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
     {{ html::style('dist/css/skins/_all-skins.min.css') }}  <!-- Morris chart -->
     {{ html::style('bower_components/morris.js/morris.css') }}
     <!-- jvectormap -->
     {{ html::style('bower_components/jvectormap/jquery-jvectormap.css') }}
     <!-- Date Picker -->
     {{ html::style('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}
     <!-- Daterange picker -->
     {{ html::style('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}
     <!-- bootstrap wysihtml5 - text editor -->
     {{ html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
     {{ html::style('css/user.css') }}



     <!-- Google Font -->
     {{ html::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') }}
     @yield('style')


 </head>
 <body class="sidebar-mini wysihtml5-supported fixed sidebar-mini-expand-feature skin-green-light ">
    <div class="wrapper">

      @extends('layouts.backend.partials.header')
      <!-- Left side column. contains the logo and sidebar -->
      @extends('layouts.backend.partials.sidebar')
  <!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
     <div class="control-sidebar-bg"></div>

 </div>


 <!-- jQuery 3 -->
 {{ html::script('bower_components/jquery/dist/jquery.min.js') }}
 <!-- jQuery UI 1.11.4 -->
 {{ html::script('bower_components/jquery-ui/jquery-ui.min.js') }}
 <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
 <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
{{ html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') }}
<!-- Morris.js charts -->
{{ html::script('bower_components/raphael/raphael.min.js') }}
{{ html::script('bower_components/morris.js/morris.min.js') }}
<!-- Sparkline -->
{{ html::script('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}
<!-- jvectormap -->
{{ html::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
{{ html::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
<!-- jQuery Knob Chart -->
{{ html::script('bower_components/jquery-knob/dist/jquery.knob.min.js') }}
<!-- daterangepicker -->
{{ html::script('bower_components/moment/min/moment.min.js') }}
{{ html::script('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}
<!-- datepicker -->
{{ html::script('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}
<!-- Bootstrap WYSIHTML5 -->
{{ html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
<!-- Slimscroll -->
{{ html::script('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}
<!-- FastClick -->
{{ html::script('bower_components/fastclick/lib/fastclick.js') }}
<!-- AdminLTE App -->
{{ html::script('dist/js/adminlte.min.js') }}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{ html::script('dist/js/pages/dashboard.js') }}
<!-- AdminLTE for demo purposes -->
{{ html::script('dist/js/demo.js') }}
{{ html::script('bower_components/ckeditor/ckeditor.js') }}
@yield('scripts')
</body>
</html>

