<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@extends('layouts.frontend.partials.head')


<body >
	<div id="app" class="index-page">
		@extends('layouts.frontend.partials.nav')

		@yield('content')
	</div>



	@extends('layouts.frontend.partials.modalContato')
	@extends('layouts.frontend.partials.modalFisioterapia')


	<!-- Scripts -->

	<!--   Core JS Files   -->
	@extends('layouts.frontend.partials.script')


</body>
</html>
