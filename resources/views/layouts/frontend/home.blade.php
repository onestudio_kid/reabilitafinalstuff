@extends('layouts.frontend.app')

@section('content')

<div class="wrapper index-page">
  @foreach ($slider as $slider)
  <div class="header header-filter" style="background-image: url('assets/Sliders/{{$slider->imagem}}');">
    <div class="container">
      <div class="row">
        <div class="col-md-6 ">
          <div class="brand">
            <h1>{{$slider->titulo}}</h1>
            <h3>{!!$slider->descricao!!}</h3>
          </div>
        </div>
      </div>

    </div>
  </div>
  @endforeach

  <div class="main main-raised">
    <div class="container ">
      <div class="section text-center section-landing ">
             {{--    <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title">Um pouco sobre Nos</h2>
                        <h5 class="description"><b>Clínica Reabilita-Fisioterapia e Tratamento,Lda</b>
                            constituída em 03/03/2008, com inicio de actividade no dia 08 de Maio de 2008
                            Estabelecimento que presta serviços de saúde , na área de fisioterapia e especialidades médicas</h5>
                        </div>
                    </div>
                    --}}
                    <div class="features" >
                      <h2 class="text-center title text-success" >Nossos Servicos</h2>
                      <div class="row " >
                        @foreach ($categorias as $categoria)

                        <div class="col-md-3">



                          <div class="info" >
                            <div class="icon icon-success">
                             <a href="{{ route('servico_detalhes',$categoria->id) }}" > <i class="material-icons" style="font-size: 3em;">content_paste</i></a>
                           </div>
                           <h4 class="info-title"><a href="{{ route('servico_detalhes',$categoria->id) }}">{{$categoria->nome}}</a></h4>

                         </div>

                       </div>
                       @endforeach
                     </div>



                   </div>
                 </div>
               </div>





               <div class="container" >
                <div class="section text-center" style="background-color: #eee">
                  <h2 class="title">Nossa Equipa</h2>

                  <div class="team">
                    <div class="row">
                     @foreach ($funcionarios as $funcionario)
                     <div class="col-md-3">
                      <div class="team-player">
                        <img src="assets/Profile/{{$funcionario->avatar}}" alt="Funcionario Image" class="img-raised img-circle">
                        <h4 class="title">  <small class="text-muted">{{($funcionario->sexo == 'F') ? 'Dr.ª': 'Dr.'}}</small>  {{$funcionario->nome}}</b> {{$funcionario->apelido}}<br />
                          <small>{{$funcionario->profissao}}</small>
                        </h4>
                 {{--    <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some <a href="#">links</a> for people to be able to follow them outside the site.</p>
                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-twitter"></i></a>
                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-instagram"></i></a>
                    <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-facebook-square"></i></a> --}}
                  </div>
                </div>
                @endforeach


              </div>
            </div>

          </div>

          <div class="section landing-section">
            <div class="row">
              <div class=" col-md-4 ">
                <ul>
                  <br>
                  <br>

                  <li><b>Adresso : </b> <small>Santo Antao - Porto Novo</small></li><br>
                  <li><b>Movel : </b> <small>+238 981-29-42</small></li><br>
                  <li><b>Telefone : </b> <small>+238 227-27-60</small></li><br>
                  <li><b>Fax : </b> <small>+238 227-27-60</small></li><br>
                  <li><b>Email : </b> <small>clinicareabilita@sapo.cv</small></li><br>
                  <li><b>Email : </b> <small>geral@clinicareabilita.com</small></li><br>
                  <br>

                </ul>
              </div>
              <div class="col-md-6 col-md-offset-2">
                <h2 class="text-center title">Deixe-nos uma Sugestão/Crítica</h2>
                {{--  <h4 class="text-center description">Divide details about your product or agency work into parts. Write a few lines about each one and contact us about any further collaboration. We will responde get back to you in a couple of hours.</h4> --}}
                <form class="contact-form " action="{{route('inbox.store')}}" method="POST">
                  {{csrf_field()}}
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating is-empty">
                        <label class="control-label">Nome</label>
                        <input type="text" class="form-control" required>
                        <span class="material-input"></span></div>
                        @if ($errors->has('nome'))
                        <strong class="red-text"><small>{{$errors->first('nome')}}</small></strong>
                        @endif
                      </div>
                      <div class="col-md-6">
                        <div class="form-group label-floating is-empty">
                          <label class="control-label">Apelido</label>
                          <input type="text" class="form-control" required>
                          <span class="material-input"></span></div>
                          @if ($errors->has('apelido'))
                          <strong class="red-text"><small>{{$errors->first('apelido')}}</small></strong>
                          @endif
                        </div>
                        <div class="col-md-6">
                          <div class="form-group label-floating is-empty">
                            <label class="control-label">Assunto</label>
                            <input type="text" class="form-control" required>
                            <span class="material-input"></span></div>
                            @if ($errors->has('assunto'))
                            <strong class="red-text"><small>{{$errors->first('assunto')}}</small></strong>
                            @endif
                          </div>
                          <div class="col-md-6">
                            <div class="form-group label-floating is-empty">
                              <label class="control-label">Email</label>
                              <input type="email" class="form-control" required>
                              <span class="material-input"></span></div>
                              @if ($errors->has('email'))
                              <strong class="red-text"><small>{{$errors->first('email')}}</small></strong>
                              @endif
                            </div>
                          </div>

                          <div class="form-group label-floating is-empty">
                            <label class="control-label">Sua Mensagem/Critica</label>
                            <textarea class="form-control" rows="4"></textarea>
                            <span class="material-input"></span>

                            @if ($errors->has('mensagem'))
                            <strong class="red-text"><small>{{$errors->first('mensagem')}}</small></strong>
                            @endif
                          </div>

                          <div class="row">
                            <div class="col-md-4 col-md-offset-4 text-center">
                              <button class="btn btn-primary btn-raised" type="submit">
                               Enviar Mensagem
                             </button>
                           </div>
                         </div>
                       </form>
                     </div>
                   </div>

                 </div>
                 {{--  --}}
               </div>


               <br>
               <br>
               <br>
               <br>

             </div>
           </div>

         </div>
       </div>


       <footer class="footer">
        <div class="container">
          <small class="pull-left">credits:</small>
          <nav class="pull-left">
            <ul>
              <li>
                <a href="http://www.onestudiocv.com" target="_blank">
                  One|studio
                </a>
              </li>
            {{--     <li>
                    <a href="http://www.facebook.com/lab.one.studio">
                     Oficial Page
                 </a>
               </li> --}}
            <!--    <li>
             <a href="http://blog.creative-tim.com">
                Blog
            </a>
          </li> -->
          <li>
           {{--  <a href="http://www.creative-tim.com/license">
                Licenses
              </a> --}}
            </li>
          </ul>
        </nav>
        <div class="copyright pull-right">
          &copy; 2018, made by:  <a href="https://www.facebook.com/kid.onestudio" target="_blank">one|studio_kid</a>
        </div>
      </div>
    </footer>

  </div>

  @endsection
