<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/dash', 'DashController@index')->name('dash');
Route::resource('slider', 'SliderController',['except' => [ 'show']]);
Route::resource('funcionario','FuncionarioController');
Route::resource('categoria','CategoriaController');
Route::resource('servico','ServicoController');
Route::resource('inbox','ContatoController');

Route::get('/about', 'AboutController@index')->name('about');
Route::get('/{detalhes}/detalhes', 'ServicoDetalhesController@show')->name('servico_detalhes');


